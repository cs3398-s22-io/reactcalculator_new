import React from 'react';
import calculate from "../logic/calculate";


describe('Calculate Tests', ()=>{
    test('Number button tests', () => {
        expect(calculate({
                total: null,
                next: null,
                operation: null},"4")).toHaveProperty('next', "4");
        expect(calculate({
                total: null,
                next: 1,
                operation: null},"1")).toHaveProperty('next', "11");
        expect(calculate({
                total: null,
                next: null,
                operation: "+"},"1")).toHaveProperty('next', "1");
    });

    test('Addition button tests', () => {
        expect(calculate({
                total: null,
                next: 1,
                operation: null},"+")).toHaveProperty('next', null);

    });

    test('Equal button tests', () => {
        expect(calculate({
                total: 2,
                next: 2,
                operation: "+"},"=")).toHaveProperty('total', "7");
        expect(calculate({
                total: 2,
                next: 21,
                operation: "+"},"=")).toHaveProperty('total', "23");
    });
});


